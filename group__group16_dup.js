var group__group16_dup =
[
    [ "The Math Behind Balancing a Ball", "group__group16.html#sec16a", [
      [ "Setting Up the Code", "group__group16.html#secSet", null ],
      [ "Balancing the Ball", "group__group16.html#secBall", null ],
      [ "The Code", "group__group16.html#secCode", null ],
      [ "The Math Behind Balancing a Ball", "group__group16.html#sec16a", [
        [ "Setting Up the Code", "group__group16.html#secSet", null ],
        [ "Balancing the Ball", "group__group16.html#secBall", null ],
        [ "The Code", "group__group16.html#secCode", null ]
      ] ]
    ] ]
];