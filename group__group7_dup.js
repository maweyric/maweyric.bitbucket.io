var group__group7_dup =
[
    [ "The Code for Week 3 of the Final Project", "group__group7.html#sec7", [
      [ "The Finite State Diagram for the Computer", "group__group7.html#FPW3S1", null ],
      [ "The Finite State Drawing for the UI task", "group__group7.html#FPW3S2", null ],
      [ "The Finite State Drawing for the Control task", "group__group7.html#FPW3S3", null ],
      [ "The Speed and Positional Graphs", "group__group7.html#FPW3S4", null ],
      [ "The Code", "group__group7.html#FPW3S5", null ],
      [ "The Code for Week 3 of the Final Project", "group__group7.html#sec7", [
        [ "The Finite State Diagram for the Computer", "group__group7.html#FPW3S1", null ],
        [ "The Finite State Drawing for the UI task", "group__group7.html#FPW3S2", null ],
        [ "The Finite State Drawing for the Control task", "group__group7.html#FPW3S3", null ],
        [ "The Speed and Positional Graphs", "group__group7.html#FPW3S4", null ],
        [ "The Code", "group__group7.html#FPW3S5", null ]
      ] ]
    ] ]
];