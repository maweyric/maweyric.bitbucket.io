var annotated_dup =
[
    [ "ADCSide", null, [
      [ "ADCcoms", "classADCSide_1_1ADCcoms.html", "classADCSide_1_1ADCcoms" ]
    ] ],
    [ "BNO055", null, [
      [ "BNO", "classBNO055_1_1BNO.html", "classBNO055_1_1BNO" ]
    ] ],
    [ "Control_task", null, [
      [ "Control_task", "classControl__task_1_1Control__task.html", "classControl__task_1_1Control__task" ]
    ] ],
    [ "Control_task_pt2", null, [
      [ "Control_task", "classControl__task__pt2_1_1Control__task.html", "classControl__task__pt2_1_1Control__task" ]
    ] ],
    [ "Control_taskpt3", null, [
      [ "Control_task", "classControl__taskpt3_1_1Control__task.html", "classControl__taskpt3_1_1Control__task" ]
    ] ],
    [ "controllerdriver", null, [
      [ "ClosedLoop", "classcontrollerdriver_1_1ClosedLoop.html", "classcontrollerdriver_1_1ClosedLoop" ]
    ] ],
    [ "cotask", null, [
      [ "Task", "classcotask_1_1Task.html", "classcotask_1_1Task" ],
      [ "TaskList", "classcotask_1_1TaskList.html", "classcotask_1_1TaskList" ]
    ] ],
    [ "encoderDriver", null, [
      [ "encoderDriver", "classencoderDriver_1_1encoderDriver.html", "classencoderDriver_1_1encoderDriver" ]
    ] ],
    [ "mainpt1", null, [
      [ "datacoms", "classmainpt1_1_1datacoms.html", "classmainpt1_1_1datacoms" ]
    ] ],
    [ "mcp9808", null, [
      [ "tempsens", "classmcp9808_1_1tempsens.html", "classmcp9808_1_1tempsens" ]
    ] ],
    [ "MotorDriver", null, [
      [ "DRV8847", "classMotorDriver_1_1DRV8847.html", "classMotorDriver_1_1DRV8847" ],
      [ "DRV8847_channel", "classMotorDriver_1_1DRV8847__channel.html", "classMotorDriver_1_1DRV8847__channel" ]
    ] ],
    [ "pyenc", null, [
      [ "pyenc", "classpyenc_1_1pyenc.html", "classpyenc_1_1pyenc" ]
    ] ],
    [ "simonclass", null, [
      [ "simonclass", "classsimonclass_1_1simonclass.html", "classsimonclass_1_1simonclass" ]
    ] ],
    [ "task_share", null, [
      [ "Queue", "classtask__share_1_1Queue.html", "classtask__share_1_1Queue" ],
      [ "Share", "classtask__share_1_1Share.html", "classtask__share_1_1Share" ]
    ] ],
    [ "touchPanelDriver", null, [
      [ "touchPanelDriver", "classtouchPanelDriver_1_1touchPanelDriver.html", "classtouchPanelDriver_1_1touchPanelDriver" ]
    ] ],
    [ "UI_task", null, [
      [ "UI_task", "classUI__task_1_1UI__task.html", "classUI__task_1_1UI__task" ]
    ] ],
    [ "UI_task_w2", null, [
      [ "UI_task", "classUI__task__w2_1_1UI__task.html", "classUI__task__w2_1_1UI__task" ]
    ] ],
    [ "UI_taskpt3", null, [
      [ "UI_task", "classUI__taskpt3_1_1UI__task.html", "classUI__taskpt3_1_1UI__task" ]
    ] ]
];