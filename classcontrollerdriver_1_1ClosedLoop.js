var classcontrollerdriver_1_1ClosedLoop =
[
    [ "__init__", "classcontrollerdriver_1_1ClosedLoop.html#aaee8c72ed9ec96e6d965834527fbbdcd", null ],
    [ "get_Kp", "classcontrollerdriver_1_1ClosedLoop.html#acf8e5b22654402257702c24d4687c469", null ],
    [ "set_Kp", "classcontrollerdriver_1_1ClosedLoop.html#a89b05a001a0b36190c03325d37e5a873", null ],
    [ "update", "classcontrollerdriver_1_1ClosedLoop.html#a970bb61970b16c5930c27fc5eb2a44ae", null ],
    [ "deltapwm", "classcontrollerdriver_1_1ClosedLoop.html#a85a9f4fa676af409c48f5f72cf77be58", null ],
    [ "kp", "classcontrollerdriver_1_1ClosedLoop.html#a8b7b45d0cb64841e97b1f3ff07598f62", null ],
    [ "newpwm", "classcontrollerdriver_1_1ClosedLoop.html#abfebe112cfa27605611ea879346392f2", null ],
    [ "pmax", "classcontrollerdriver_1_1ClosedLoop.html#aa3c4b4f4994f48f0216f616f794ff218", null ],
    [ "pmin", "classcontrollerdriver_1_1ClosedLoop.html#a4015c0e63195e46826e1f33f0bc1df88", null ],
    [ "pwm", "classcontrollerdriver_1_1ClosedLoop.html#a487cd053c4ffb882128916aec714ecf4", null ],
    [ "speedcurrent", "classcontrollerdriver_1_1ClosedLoop.html#adb5f5e0c18f1f7d75c9cf7c338f200e3", null ],
    [ "speedreq", "classcontrollerdriver_1_1ClosedLoop.html#a1ea585e56705fa12b3f45aa6a9c678dd", null ]
];