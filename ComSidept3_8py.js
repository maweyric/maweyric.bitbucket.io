var ComSidept3_8py =
[
    [ "kb_cb", "ComSidept3_8py.html#adf33f0810cf6e9f1cbe2167006c4e830", null ],
    [ "delta", "ComSidept3_8py.html#ab405bf000da6c479842d5c532359062f", null ],
    [ "finished", "ComSidept3_8py.html#a315cd327fa5d7b3947f6c8e9cf12ea5e", null ],
    [ "inv", "ComSidept3_8py.html#a4f7f526da7bf780a91ef113a8eb585ea", null ],
    [ "kp", "ComSidept3_8py.html#a29c69d8ea1d8b0b286ce03aaebb6508f", null ],
    [ "last_key", "ComSidept3_8py.html#aada486cd37e9702b75e65ba1c5c5fc9a", null ],
    [ "passcode", "ComSidept3_8py.html#ac20904dd4071a50c2d7aff2093ee1020", null ],
    [ "polished_data", "ComSidept3_8py.html#a308e89e0025f0ebb83b339375292e864", null ],
    [ "pos", "ComSidept3_8py.html#aa2f698678e7d271f0b262a94b7401f27", null ],
    [ "raw_data", "ComSidept3_8py.html#aa780ed30ed5b0cbb685cc911a0afaee8", null ],
    [ "ready", "ComSidept3_8py.html#a9324c242b36a348df8bd12bacab2bd63", null ],
    [ "rough_data", "ComSidept3_8py.html#a49634745dbcfa13fc6e30896a20eb80a", null ],
    [ "rpm", "ComSidept3_8py.html#aae87a46d60f2bb72ab5ac2b7d12b1a6f", null ],
    [ "ser", "ComSidept3_8py.html#ac0f5bf54ed7e815d77f0687bcb5b5324", null ],
    [ "speed", "ComSidept3_8py.html#a10d2255373f1738b72bc9c891554fb39", null ],
    [ "state", "ComSidept3_8py.html#a062036f98e90fae05d1b82efe1d759e9", null ],
    [ "times", "ComSidept3_8py.html#af1f6369e81f8e29987a834389c06a959", null ],
    [ "usr", "ComSidept3_8py.html#ad3bdd8117d7ce2f11bbe28ba525fe49e", null ],
    [ "values", "ComSidept3_8py.html#acae10344c4c0d3fff7b32f05b4bfaae5", null ],
    [ "writer", "ComSidept3_8py.html#aba0bd301134bb1bf6053b2eb0717f748", null ]
];