var classsimonclass_1_1simonclass =
[
    [ "__init__", "classsimonclass_1_1simonclass.html#a5c166d1080b9d264280114a1e185a2f3", null ],
    [ "compare", "classsimonclass_1_1simonclass.html#ac89478632fd6ad04bd5cd2e3f49ef079", null ],
    [ "lights", "classsimonclass_1_1simonclass.html#a2116d90bcb944a969911c0794fb49e82", null ],
    [ "pattern", "classsimonclass_1_1simonclass.html#a56bd3369d5d2ee833f72d2c6f41b2fca", null ],
    [ "run", "classsimonclass_1_1simonclass.html#a5c8ed01bab711c9118e480b6b656fbfa", null ],
    [ "difficulty", "classsimonclass_1_1simonclass.html#ab05fe509477fae224c9b6c2c3a3ea3fb", null ],
    [ "LED", "classsimonclass_1_1simonclass.html#a1b992ef3f13924bfbc65c39be385e8a3", null ],
    [ "LEDbright", "classsimonclass_1_1simonclass.html#a2480a78d2a0fc8eb17b7ce6ea05c8f02", null ],
    [ "loss", "classsimonclass_1_1simonclass.html#a3812feed44eadd22c5d5743ff6dee44c", null ],
    [ "pat", "classsimonclass_1_1simonclass.html#a106d597d1bfea95ea4532e23915fc23f", null ],
    [ "pressBatch", "classsimonclass_1_1simonclass.html#ad8a281b406cc81fb18d2623b0a43666b", null ],
    [ "tim2", "classsimonclass_1_1simonclass.html#a5ff27bf62c6c45865176e66de206448e", null ],
    [ "win", "classsimonclass_1_1simonclass.html#af7ae2689d33da8ef9fb92c5a529cba0d", null ]
];