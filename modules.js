var modules =
[
    [ "Lab 1.1", "group__group1.html", "group__group1" ],
    [ "Lab 1.2", "group__group2.html", "group__group2" ],
    [ "HW 1.3", "group__group3.html", "group__group3" ],
    [ "Lab 1.3", "group__group4.html", "group__group4" ],
    [ "Final Project Week 1", "group__group5.html", "group__group5" ],
    [ "Final Project Week 2", "group__group6.html", "group__group6" ],
    [ "Final Project Week 3", "group__group7.html", "group__group7" ],
    [ "Final Project Week 4", "group__group8.html", "group__group8" ],
    [ "Lab 2.1", "group__group9.html", "group__group9" ],
    [ "Lab 2.2", "group__group10.html", "group__group10" ],
    [ "Final Project 2 Math", "group__group11.html", "group__group11" ],
    [ "Lab 2.3", "group__group12.html", "group__group12" ],
    [ "Final project 2 Linearization of System", "group__group13.html", "group__group13" ],
    [ "Lab 2.4", "group__group14.html", "group__group14" ],
    [ "Final project 2 Finding Gains", "group__group15.html", "group__group15" ],
    [ "Final Project 2", "group__group16.html", "group__group16" ]
];