var sharespt3_8py =
[
    [ "act_rpm", "sharespt3_8py.html#a1c2d08f427eb03f52b227e1a137f6ded", null ],
    [ "Data_send", "sharespt3_8py.html#aadaf797407b79ef717447693c0021306", null ],
    [ "delta", "sharespt3_8py.html#ab8d803659b8b9ba67ac6749fb880c04c", null ],
    [ "des_rpm", "sharespt3_8py.html#a4a482f9d578b0af273002d9a14440c75", null ],
    [ "kp", "sharespt3_8py.html#a45daff904a177a39baece42ca5f4534c", null ],
    [ "kp_get", "sharespt3_8py.html#a4734b5b6b0afbf1354ed7842b2494c40", null ],
    [ "loop", "sharespt3_8py.html#aca7ee88514be1178e6675a34a77247d9", null ],
    [ "pos", "sharespt3_8py.html#a78af7c33cb76e95293d17f7d4e49cf57", null ],
    [ "position", "sharespt3_8py.html#ae7337de3c958fc0a06cb7af8ac477b13", null ],
    [ "pwm", "sharespt3_8py.html#a05f16348455d5d78f6f89b05e12e43b9", null ],
    [ "rpm", "sharespt3_8py.html#a130f872fbde629080b21b51c2ffe8e82", null ],
    [ "rpm_get", "sharespt3_8py.html#abc6642979939081c57ef6e25594e6b1c", null ],
    [ "speed", "sharespt3_8py.html#a91b4948a5c4a2499a8eddec38652fa04", null ],
    [ "speed_pos", "sharespt3_8py.html#ab37f7f059dea688d5dd85a2e31ec8d57", null ],
    [ "speed_tims", "sharespt3_8py.html#ac07736d49e6eb7990e24d072f7715f83", null ],
    [ "tims", "sharespt3_8py.html#ab66e058bb86d03194bbea070fcfe4f4f", null ],
    [ "User_Input", "sharespt3_8py.html#acff2d9eb8a316ae0cbeb6b59b7002e49", null ]
];