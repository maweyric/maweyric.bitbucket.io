var ComSide__pt2_8py =
[
    [ "kb_cb", "ComSide__pt2_8py.html#ac036d0247f6086dc33697432ecb310a8", null ],
    [ "delta", "ComSide__pt2_8py.html#afc9e79269a7b0b4927fa6d2a8c29da4f", null ],
    [ "finished", "ComSide__pt2_8py.html#a053ee280ebea0c5855f75c55fc63eae4", null ],
    [ "inv", "ComSide__pt2_8py.html#ad26a83ff287bc1780d8392a489b79ebd", null ],
    [ "last_key", "ComSide__pt2_8py.html#a9e25ee3be68faf9a244ddf6fad06b43c", null ],
    [ "passcode", "ComSide__pt2_8py.html#a46804325c606f6fab48883420cf638f3", null ],
    [ "polished_data", "ComSide__pt2_8py.html#adbf920fc09122e03e65f9a438ea9157f", null ],
    [ "pos", "ComSide__pt2_8py.html#a1a9e7bb35ed4a1bacdfdb77efc8a5b80", null ],
    [ "raw_data", "ComSide__pt2_8py.html#add01b7e6ac4c1f666267cc4f89ea116a", null ],
    [ "ready", "ComSide__pt2_8py.html#a44602b06818d2624af9e70875ba1c98a", null ],
    [ "rough_data", "ComSide__pt2_8py.html#a9612d7f3a1d31e541ac456e79fb68926", null ],
    [ "ser", "ComSide__pt2_8py.html#a312ec50375275d02b4d3b0bbb610b264", null ],
    [ "state", "ComSide__pt2_8py.html#a536296328b4e26c929d1986007adadb4", null ],
    [ "times", "ComSide__pt2_8py.html#a63331cc08aa70612a8bc5ed99721699e", null ],
    [ "usr", "ComSide__pt2_8py.html#a248155fa2bbdd816bed67031d9a719c1", null ],
    [ "values", "ComSide__pt2_8py.html#ad993533913b2c60a939d8847bfa71ee6", null ],
    [ "writer", "ComSide__pt2_8py.html#a436c739b12e39b94d1a5315d3158fb5c", null ]
];