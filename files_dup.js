var files_dup =
[
    [ "ADCSide.py", "ADCSide_8py.html", "ADCSide_8py" ],
    [ "BNO055.py", "BNO055_8py.html", [
      [ "BNO", "classBNO055_1_1BNO.html", "classBNO055_1_1BNO" ]
    ] ],
    [ "BotUpFib.py", "BotUpFib_8py.html", "BotUpFib_8py" ],
    [ "ChangeFunc.py", "ChangeFunc_8py.html", "ChangeFunc_8py" ],
    [ "ComSide.py", "ComSide_8py.html", "ComSide_8py" ],
    [ "ComSide_pt2.py", "ComSide__pt2_8py.html", "ComSide__pt2_8py" ],
    [ "ComSidept3.py", "ComSidept3_8py.html", "ComSidept3_8py" ],
    [ "Control_task.py", "Control__task_8py.html", [
      [ "Control_task", "classControl__task_1_1Control__task.html", "classControl__task_1_1Control__task" ]
    ] ],
    [ "Control_task_pt2.py", "Control__task__pt2_8py.html", [
      [ "Control_task", "classControl__task__pt2_1_1Control__task.html", "classControl__task__pt2_1_1Control__task" ]
    ] ],
    [ "Control_taskpt3.py", "Control__taskpt3_8py.html", [
      [ "Control_task", "classControl__taskpt3_1_1Control__task.html", "classControl__taskpt3_1_1Control__task" ]
    ] ],
    [ "controllerdriver.py", "controllerdriver_8py.html", [
      [ "ClosedLoop", "classcontrollerdriver_1_1ClosedLoop.html", "classcontrollerdriver_1_1ClosedLoop" ]
    ] ],
    [ "ControllerTask.py", "ControllerTask_8py.html", "ControllerTask_8py" ],
    [ "cotask.py", "cotask_8py.html", "cotask_8py" ],
    [ "CSide.py", "CSide_8py.html", "CSide_8py" ],
    [ "dataTask.py", "dataTask_8py.html", "dataTask_8py" ],
    [ "encoderDriver.py", "encoderDriver_8py.html", [
      [ "encoderDriver", "classencoderDriver_1_1encoderDriver.html", "classencoderDriver_1_1encoderDriver" ]
    ] ],
    [ "encoderTask.py", "encoderTask_8py.html", "encoderTask_8py" ],
    [ "FiniteLED.py", "FiniteLED_8py.html", "FiniteLED_8py" ],
    [ "HW3elevator.py", "HW3elevator_8py.html", "HW3elevator_8py" ],
    [ "IMUTask.py", "IMUTask_8py.html", "IMUTask_8py" ],
    [ "main.py", "main_8py.html", "main_8py" ],
    [ "mainL24.py", "mainL24_8py.html", "mainL24_8py" ],
    [ "mainpt1.py", "mainpt1_8py.html", "mainpt1_8py" ],
    [ "mainpt3.py", "mainpt3_8py.html", "mainpt3_8py" ],
    [ "mcp9808.py", "mcp9808_8py.html", [
      [ "tempsens", "classmcp9808_1_1tempsens.html", "classmcp9808_1_1tempsens" ]
    ] ],
    [ "MotorDriver.py", "MotorDriver_8py.html", "MotorDriver_8py" ],
    [ "MotorTask.py", "MotorTask_8py.html", "MotorTask_8py" ],
    [ "myFile.py", "myFile_8py.html", "myFile_8py" ],
    [ "print_task.py", "print__task_8py.html", "print__task_8py" ],
    [ "pyenc.py", "pyenc_8py.html", [
      [ "pyenc", "classpyenc_1_1pyenc.html", "classpyenc_1_1pyenc" ]
    ] ],
    [ "SerialTest.py", "SerialTest_8py.html", "SerialTest_8py" ],
    [ "shares.py", "shares_8py.html", "shares_8py" ],
    [ "shares_pt2.py", "shares__pt2_8py.html", "shares__pt2_8py" ],
    [ "sharespt3.py", "sharespt3_8py.html", "sharespt3_8py" ],
    [ "SimonSays.py", "SimonSays_8py.html", "SimonSays_8py" ],
    [ "task_share.py", "task__share_8py.html", "task__share_8py" ],
    [ "ThinkFastA.py", "ThinkFastA_8py.html", "ThinkFastA_8py" ],
    [ "ThinkFastB.py", "ThinkFastB_8py.html", "ThinkFastB_8py" ],
    [ "touchPanelDriver.py", "touchPanelDriver_8py.html", [
      [ "touchPanelDriver", "classtouchPanelDriver_1_1touchPanelDriver.html", "classtouchPanelDriver_1_1touchPanelDriver" ]
    ] ],
    [ "touchPanelTask.py", "touchPanelTask_8py.html", "touchPanelTask_8py" ],
    [ "UI_task.py", "UI__task_8py.html", [
      [ "UI_task", "classUI__task_1_1UI__task.html", "classUI__task_1_1UI__task" ]
    ] ],
    [ "UI_task_w2.py", "UI__task__w2_8py.html", [
      [ "UI_task", "classUI__task__w2_1_1UI__task.html", "classUI__task__w2_1_1UI__task" ]
    ] ],
    [ "UI_taskpt3.py", "UI__taskpt3_8py.html", [
      [ "UI_task", "classUI__taskpt3_1_1UI__task.html", "classUI__taskpt3_1_1UI__task" ]
    ] ]
];